Title: CGI-Simple update needs --permit-downgrade
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2018-09-09
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-perl/CGI-Simple[>=1.99]

dev-perl/CGI-Simple's upstream failed to use continuous version numbers,
resulting in the need to forcefully downgrade the package to install the
update. You can do that via

# cave resolve -1 dev-perl/CGI-Simple --permit-downgrade dev-perl/CGI-Simple
